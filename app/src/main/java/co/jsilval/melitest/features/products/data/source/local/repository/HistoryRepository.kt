package co.jsilval.melitest.features.products.data.source.local.repository

import co.jsilval.melitest.core.Result
import co.jsilval.melitest.features.products.view.adapter.history.viewmodels.HistoryViewModel
import kotlinx.coroutines.flow.Flow

interface HistoryRepository {
    suspend fun fetchHistory(query: String): Flow<Result<List<HistoryViewModel>, String>>

    suspend fun saveHistory(query: String): Flow<Result<String, String>>
}