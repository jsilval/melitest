package co.jsilval.melitest.features.detail.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import co.jsilval.melitest.R
import co.jsilval.melitest.core.common.dialogs.ErrorType
import co.jsilval.melitest.core.common.extension.showToast
import co.jsilval.melitest.core.common.fragments.BaseFragment
import co.jsilval.melitest.core.common.fragments.getDialogsManager
import co.jsilval.melitest.core.common.viewmodels.ViewModelFactory
import co.jsilval.melitest.databinding.ProductDetailFragmentBinding
import co.jsilval.melitest.features.detail.domain.entities.ProductDetail
import co.jsilval.melitest.features.detail.viewmodel.ProductDetailViewModel
import co.jsilval.melitest.features.main.ui.dialogs.ErrorDialog
import com.bumptech.glide.Glide
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

/**
 * Detalle de un producto. Recibe el id de producto al cual se va a mostrar el detalle
 */
class ProductDetailFragment : BaseFragment(), ErrorDialog.OnErrorDialogListener {

    @Inject
    lateinit var mViewModelFactory: ViewModelFactory

    private val viewModel by viewModels<ProductDetailViewModel> { mViewModelFactory }
    private var _binding: ProductDetailFragmentBinding? = null
    private val binding get() = _binding!!

    private val args: ProductDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ProductDetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        getProduct()
    }

    /**
     * Indica que se deben obtener los datos de un producto según un Id
     */
    private fun getProduct() {
        viewModel.getProductById(args.productId)
    }

    /**
     * Configura la toolbar del fragmento
     */
    private fun setupToolbar() {
        binding.tbDetail.setNavigationOnClickListener {
            goTo(ProductDetailFragmentDirections.toProductsFragment(), R.id.productsFragment)
        }
    }

    /**
     * Método para agregar observadores sobre datos del viewmodel
     */
    override fun addObservers() {
        observeState()
        observeProduct()
    }

    /**
     * Se observan los cambios en el estado de la vista. Atado al ciclo de vida del fragmento.
     * Se reciben cada cambio de estado de la vista para luego ser manejado según el nuevo estado.
     */
    private fun observeState() {
        viewModel.state
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { state ->
                handleState(state)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    /**
     * Se observan los cambios en los valores del producto a mostrar.  Atado al ciclo de vida del fragmento.
     * Cada vez que se consulta un producto por id se reciben nuevos datos y cada uno se utiliza para
     * mostrar información sobre la vista.
     */
    private fun observeProduct() {
        viewModel.products
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { productDetail ->
                handleProductSuccess(productDetail)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    /**
     * Muestra los datos del producto en la vista si el producto es válido
     */
    private fun handleProductSuccess(productDetail: ProductDetail?) {
        if (productDetail != null) {
            Glide.with(this).load(productDetail.thumbnail).into(binding.ivProduct)
            binding.tvDescription.text = productDetail.detail
            binding.tvPrice.text = productDetail.price
            binding.tvProductName.text = productDetail.title
        }
    }

    /**
     * Maneja los estados de la vista
     *
     * @param state inidica el nuevo estado de la vista
     */
    private fun handleState(state: ProductDetailViewModel.ProductDetailFragmentState) {
        when (state) {
            ProductDetailViewModel.ProductDetailFragmentState.Init -> Unit
            is ProductDetailViewModel.ProductDetailFragmentState.IsLoading -> handleLoading(state.isLoading)
            is ProductDetailViewModel.ProductDetailFragmentState.ShowToast -> requireActivity().showToast(
                state.message
            )
            is ProductDetailViewModel.ProductDetailFragmentState.ShowErrorDialog -> handleErrorDialogs(
                state.errorType
            )
        }
    }

    /**
     * Manejador del estado IsLoading
     */
    private fun handleLoading(isLoading: Boolean) {
        when (isLoading) {
            true -> showLoadingProgressBar()
            false -> hideLoadingProgressBar()
        }
    }

    /**
     * Oculta la barra de progreso sobre la toolbar
     */
    private fun hideLoadingProgressBar() {
        binding.progressBar.visibility = View.GONE
    }

    /**
     * Muestra la barra de progreso sobre la toolbar
     */
    private fun showLoadingProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    /**
     * Manejador de tipos de error que podrían ocurrir
     */
    private fun handleErrorDialogs(errorType: ErrorType) {
        when (errorType) {
            ErrorType.ERROR -> showErrorDialog()
            ErrorType.EMPTY_RESULT -> showErrorDialog()
            ErrorType.NO_INTERNET -> showErrorDialog()
        }
    }

    /**
     * Muestra un diálogo de error
     */
    private fun showErrorDialog() {
        getDialogsManager().showDialogWithId(ErrorDialog(), "error_dialog")
    }

    /**
     * Devuelve a la lista de productos en caso de que haya ocurrido un error
     */
    override fun onDismissErrorDialog() {
        goTo(ProductDetailFragmentDirections.toProductsFragment(), R.id.productsFragment)
    }
}