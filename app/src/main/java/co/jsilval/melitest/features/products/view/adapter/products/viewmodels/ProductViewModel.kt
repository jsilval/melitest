package co.jsilval.melitest.features.products.view.adapter.products.viewmodels

import co.jsilval.melitest.features.products.view.adapter.products.entities.Product
import co.jsilval.melitest.features.products.view.adapter.products.factory.ProductsTypeFactory

class ProductViewModel(var product: Product) : AbstractViewModel() {

    init {
        id = product.id
    }

    override fun type(typeFactory: ProductsTypeFactory): Int {
        return typeFactory.type(product)
    }
}
