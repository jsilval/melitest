package co.jsilval.melitest.features.products.domain.usecase

import co.jsilval.melitest.core.Result
import co.jsilval.melitest.features.products.data.source.local.repository.HistoryRepositoryImpl
import co.jsilval.melitest.features.products.view.adapter.history.viewmodels.HistoryViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * Caso de uso para manejar lo relacionado con el historial de búsquedas
 */
class HistoryByQuery @Inject constructor(private val historyRepository: HistoryRepositoryImpl) {

    suspend fun getHistory(query: String): Flow<Result<List<HistoryViewModel>, String>> {
        return historyRepository.fetchHistory(query)
    }

    suspend fun saveQuery(query: String): Flow<Result<String, String>> {
        return historyRepository.saveHistory(query)
    }
}