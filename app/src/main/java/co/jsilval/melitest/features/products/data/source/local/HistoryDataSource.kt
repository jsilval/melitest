package co.jsilval.melitest.features.products.data.source.local

import co.jsilval.melitest.core.db.history.History
import co.jsilval.melitest.core.db.history.HistoryDao
import javax.inject.Inject

/**
 * Fuente de datos local para sacar el historial de la bd
 */
class HistoryDataSource @Inject constructor(private var historyDao: HistoryDao) {

    fun getHistory(query: String) = historyDao.getHistoryList(query)

    fun saveHistory(query: String) = historyDao.insert(History(query))
}