package co.jsilval.melitest.features.products.data.source.local.repository

import co.jsilval.melitest.core.Result
import co.jsilval.melitest.core.common.dialogs.ErrorType
import co.jsilval.melitest.features.detail.domain.entities.ProductDetail
import co.jsilval.melitest.features.products.data.source.local.ProductsLocalDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

/**
 * Repositorio de datos locales para obtener los productos
 */
class ProductsLocalRepositoryImpl @Inject constructor(private val productsLocalDataSource: ProductsLocalDataSource) :
    ProductsLocalRepository {

    override suspend fun getProductById(id: String): Flow<Result<ProductDetail, ErrorType>> {
        return flow {
            val product = productsLocalDataSource.getProductById(id)
            if (product != null) {
                val detail = product.toProductDetailModel()
                emit(Result.Success(detail))
            } else {
                emit(Result.Error(ErrorType.EMPTY_RESULT))
            }
        }.flowOn(Dispatchers.IO)
    }
}