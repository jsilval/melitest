package co.jsilval.melitest.features.products.data.source.local.repository

import co.jsilval.melitest.core.Result
import co.jsilval.melitest.features.products.data.source.local.HistoryDataSource
import co.jsilval.melitest.features.products.view.adapter.history.viewmodels.HistoryViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

/**
 * Respositorio de datos local para obtener el historial de búsqueda
 */
class HistoryRepositoryImpl @Inject constructor(private val historyDataSource: HistoryDataSource) :
    HistoryRepository {

    override suspend fun fetchHistory(query: String): Flow<Result<List<HistoryViewModel>, String>> {
        return flow {
            val history = historyDataSource.getHistory(query).map {
                it.toDomainModelHistory()
            }
            emit(Result.Success(history))
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun saveHistory(query: String): Flow<Result<String, String>> {
        return flow {
            val rowId = historyDataSource.saveHistory(query)
            if (rowId > 0) {
                emit(Result.Success(query))
            } else {
                emit(Result.Error(""))
            }
        }.flowOn(Dispatchers.IO)
    }
}