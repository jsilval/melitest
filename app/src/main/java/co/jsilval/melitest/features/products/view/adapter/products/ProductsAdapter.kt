package co.jsilval.melitest.features.products.view.adapter.products

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.jsilval.melitest.features.products.view.adapter.products.entities.Product
import co.jsilval.melitest.features.products.view.adapter.products.factory.ProductsTypeFactory
import co.jsilval.melitest.features.products.view.adapter.products.factory.ProductsTypeFactoryForList
import co.jsilval.melitest.features.products.view.adapter.products.viewholder.AbstractViewHolder
import co.jsilval.melitest.features.products.view.adapter.products.viewmodels.AbstractViewModel
import co.jsilval.melitest.features.products.view.adapter.products.viewmodels.ProductViewModel
import kotlin.properties.Delegates

class ProductsAdapter(
    private val typeFactory: ProductsTypeFactory = ProductsTypeFactoryForList(),
    val listener: OnItemClickListener
) :
    RecyclerView.Adapter<AbstractViewHolder<AbstractViewModel>>(),
    AbstractViewHolder.OnItemClickedListener {

    var items: List<AbstractViewModel> by Delegates.observable(ArrayList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    lateinit var context: Context

    override fun onBindViewHolder(holder: AbstractViewHolder<AbstractViewModel>, position: Int) {
        val item = items[position]
        holder.bind(item)
        holder.setOnItemClickListener(this)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder<AbstractViewModel> {
        context = parent.context
        val contactView = LayoutInflater.from(context).inflate(viewType, parent, false)
        return typeFactory.createViewHolder(
            contactView,
            viewType
        ) as AbstractViewHolder<AbstractViewModel>
    }

    override fun getItemViewType(position: Int): Int = items[position].type(typeFactory)

    override fun getItemCount() = items.count()

    override fun onClickProductItem(adapterPosition: Int) {
        listener.onClickProductItem((items[adapterPosition] as ProductViewModel).product)
    }

    interface OnItemClickListener {
        fun onClickProductItem(product: Product)
    }
}

