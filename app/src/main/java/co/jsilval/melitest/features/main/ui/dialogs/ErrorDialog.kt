package co.jsilval.melitest.features.main.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.jsilval.melitest.core.common.dialogs.BaseFullScreenDialog
import co.jsilval.melitest.databinding.ErrorStateLayoutBinding
import co.jsilval.melitest.features.main.ui.MainActivity

class ErrorDialog : BaseFullScreenDialog() {

    private val binding get() = _binding!!
    private var _binding: ErrorStateLayoutBinding? = null

    private var listener: OnErrorDialogListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
        setListener()
    }

    override fun onResume() {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        super.onResume()
    }

    private fun setListener() {
        val primaryNavigationFragment =
            (context as MainActivity).supportFragmentManager.primaryNavigationFragment
        val fragment = primaryNavigationFragment?.childFragmentManager?.fragments?.get(0)
        if (fragment is OnErrorDialogListener) {
            listener = fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ErrorStateLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.btnRetry.setOnClickListener {
            dismissAllowingStateLoss()
            listener?.onDismissErrorDialog()
        }
    }

    interface OnErrorDialogListener {
        fun onDismissErrorDialog()
    }
}