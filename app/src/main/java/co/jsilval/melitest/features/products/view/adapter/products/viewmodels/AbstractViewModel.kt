package co.jsilval.melitest.features.products.view.adapter.products.viewmodels

import co.jsilval.melitest.features.products.view.adapter.products.factory.ProductsTypeFactory

abstract class AbstractViewModel(var id: String? = null) {
    abstract fun type(typeFactory: ProductsTypeFactory): Int

    override fun equals(other: Any?): Boolean {
        if (id == null || other == null) {
            return super.equals(other)
        }

        return (other as AbstractViewModel).id == id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}