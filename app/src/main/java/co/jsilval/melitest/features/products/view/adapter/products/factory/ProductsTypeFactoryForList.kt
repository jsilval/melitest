package co.jsilval.melitest.features.products.view.adapter.products.factory

import android.view.View
import co.jsilval.melitest.features.products.view.adapter.products.entities.Product
import co.jsilval.melitest.features.products.view.adapter.products.exception.TypeNotSupportedException
import co.jsilval.melitest.features.products.view.adapter.products.viewholder.AbstractViewHolder
import co.jsilval.melitest.features.products.view.adapter.products.viewholder.ProductViewHolder

class ProductsTypeFactoryForList : ProductsTypeFactory {

    override fun type(product: Product): Int = ProductViewHolder.LAYOUT

    override fun createViewHolder(parent: View, type: Int): AbstractViewHolder<*> {
        return when (type) {
            ProductViewHolder.LAYOUT -> ProductViewHolder(parent)
            else -> throw TypeNotSupportedException.create(String.format("LayoutType: %d", type))
        }
    }
}
