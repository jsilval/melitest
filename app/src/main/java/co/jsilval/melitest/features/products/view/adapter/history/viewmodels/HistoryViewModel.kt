package co.jsilval.melitest.features.products.view.adapter.history.viewmodels

import co.jsilval.melitest.features.products.view.adapter.history.entities.History
import co.jsilval.melitest.features.products.view.adapter.history.factory.SearchHistoryTypeFactory

class HistoryViewModel(var history: History) : AbstractViewModel() {

    init {
        id = history.data
    }

    override fun type(typeFactory: SearchHistoryTypeFactory): Int {
        return typeFactory.type(history)
    }
}
