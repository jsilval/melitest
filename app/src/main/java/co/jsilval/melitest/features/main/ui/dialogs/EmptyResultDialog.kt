package co.jsilval.melitest.features.main.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.jsilval.melitest.core.common.dialogs.BaseFullScreenDialog
import co.jsilval.melitest.databinding.EmptyStateItemLayoutBinding
import co.jsilval.melitest.features.main.ui.MainActivity

class EmptyResultDialog : BaseFullScreenDialog() {

    private var _binding: EmptyStateItemLayoutBinding? = null
    private val binding get() = _binding!!

    private var listener: OnEmptyResultDialogListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
        setListener()
    }

    override fun onResume() {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        super.onResume()
    }

    private fun setListener() {
        val primaryNavigationFragment =
            (context as MainActivity).supportFragmentManager.primaryNavigationFragment
        val fragment = primaryNavigationFragment?.childFragmentManager?.fragments?.get(0)
        if (fragment is OnEmptyResultDialogListener) {
            listener = fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = EmptyStateItemLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.btnRetry.setOnClickListener {
            dismissAllowingStateLoss()
            listener?.onDismissEmptyResultDialog()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    interface OnEmptyResultDialogListener {
        fun onDismissEmptyResultDialog()
    }
}