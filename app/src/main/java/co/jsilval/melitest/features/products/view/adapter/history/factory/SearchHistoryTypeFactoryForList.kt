package co.jsilval.melitest.features.products.view.adapter.history.factory

import android.view.View
import co.jsilval.melitest.features.products.view.adapter.history.entities.History
import co.jsilval.melitest.features.products.view.adapter.history.exception.TypeNotSupportedException
import co.jsilval.melitest.features.products.view.adapter.history.viewholder.AbstractViewHolder
import co.jsilval.melitest.features.products.view.adapter.history.viewholder.HistoryViewHolder

class SearchHistoryTypeFactoryForList : SearchHistoryTypeFactory {

    override fun type(history: History): Int = HistoryViewHolder.LAYOUT

    override fun createViewHolder(parent: View, type: Int): AbstractViewHolder<*> {
        return when (type) {
            HistoryViewHolder.LAYOUT -> HistoryViewHolder(parent)
            else -> throw TypeNotSupportedException.create(String.format("LayoutType: %d", type))
        }
    }
}
