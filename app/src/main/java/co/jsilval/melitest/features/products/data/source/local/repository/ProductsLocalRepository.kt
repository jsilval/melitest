package co.jsilval.melitest.features.products.data.source.local.repository

import co.jsilval.melitest.core.Result
import co.jsilval.melitest.core.common.dialogs.ErrorType
import co.jsilval.melitest.features.detail.domain.entities.ProductDetail
import kotlinx.coroutines.flow.Flow

interface ProductsLocalRepository {
    suspend fun getProductById(id: String): Flow<Result<ProductDetail, ErrorType>>
}