package co.jsilval.melitest.features.products.data.source.local

import co.jsilval.melitest.core.db.products.ProductDao
import javax.inject.Inject

/**
 * Fuente de datos local para obtener productos
 */
class ProductsLocalDataSource @Inject constructor(private val productDao: ProductDao) {

    fun getProductById(id: String) = productDao.getProductById(id)
}