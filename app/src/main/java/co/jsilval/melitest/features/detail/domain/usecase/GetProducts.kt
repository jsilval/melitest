package co.jsilval.melitest.features.detail.domain.usecase

import co.jsilval.melitest.core.Result
import co.jsilval.melitest.core.common.dialogs.ErrorType
import co.jsilval.melitest.features.detail.domain.entities.ProductDetail
import co.jsilval.melitest.features.products.data.source.local.repository.ProductsLocalRepositoryImpl
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * Caso de uso para obtener productos de un repositorio local
 */
class GetProducts @Inject constructor(private val productsLocalRepository: ProductsLocalRepositoryImpl) {

    suspend fun byId(id: String): Flow<Result<ProductDetail, ErrorType>> {
        return productsLocalRepository.getProductById(id)
    }
}