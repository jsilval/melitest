package co.jsilval.melitest.features.products.data.source.remote.repository

import co.jsilval.melitest.core.Result
import co.jsilval.melitest.core.common.dialogs.ErrorType
import co.jsilval.melitest.features.products.data.source.remote.ProductsRemoteDataSource
import co.jsilval.melitest.features.products.view.adapter.products.viewmodels.ProductViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * Respositorio que consulta una fuente de datos remota que devuelve productos
 */
@ExperimentalCoroutinesApi
class ProductsRemoteRepositoryImpl @Inject constructor(private val productsRemoteDataSource: ProductsRemoteDataSource) :
    ProductsRemoteRepository {

    override suspend fun fetchProducts(query: String): Flow<Result<List<ProductViewModel>, ErrorType>> {
        return productsRemoteDataSource.getProductsHistory(query)
    }
}