package co.jsilval.melitest.features.products.data.source.remote.repository

import co.jsilval.melitest.core.Result
import co.jsilval.melitest.core.common.dialogs.ErrorType
import co.jsilval.melitest.features.products.view.adapter.products.viewmodels.ProductViewModel
import kotlinx.coroutines.flow.Flow

interface ProductsRemoteRepository {
    suspend fun fetchProducts(query: String): Flow<Result<List<ProductViewModel>, ErrorType>>
}