package co.jsilval.melitest.features.products.view.adapter.history.factory

import android.view.View
import co.jsilval.melitest.features.products.view.adapter.history.entities.History
import co.jsilval.melitest.features.products.view.adapter.history.viewholder.AbstractViewHolder

interface SearchHistoryTypeFactory {

    fun type(history: History): Int

    fun createViewHolder(parent: View, type: Int): AbstractViewHolder<*>
}
