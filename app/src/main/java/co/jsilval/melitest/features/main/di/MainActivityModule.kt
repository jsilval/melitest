package co.jsilval.melitest.features.main.di

import androidx.fragment.app.FragmentManager
import co.jsilval.melitest.features.main.ui.MainActivity
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.ExperimentalCoroutinesApi

@Module
class MainActivityModule {
    companion object {
        @ExperimentalCoroutinesApi
        @Provides
        fun provideFragmentManager(movieDetailFragment: MainActivity): FragmentManager {
            return movieDetailFragment.supportFragmentManager
        }
    }
}