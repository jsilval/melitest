package co.jsilval.melitest.features.main.ui

import android.os.Bundle
import co.jsilval.melitest.core.common.activities.BaseActivity
import co.jsilval.melitest.databinding.ActivityMainBinding

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}