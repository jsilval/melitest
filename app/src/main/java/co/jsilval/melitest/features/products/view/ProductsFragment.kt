package co.jsilval.melitest.features.products.view

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.core.content.ContextCompat.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import co.jsilval.melitest.R
import co.jsilval.melitest.core.common.dialogs.ErrorType
import co.jsilval.melitest.core.common.dialogs.LoadingDialog
import co.jsilval.melitest.core.common.extension.showToast
import co.jsilval.melitest.core.common.fragments.BaseFragment
import co.jsilval.melitest.core.common.fragments.getDialogsManager
import co.jsilval.melitest.core.common.fragments.hideKeyboard
import co.jsilval.melitest.core.common.fragments.showKeyboard
import co.jsilval.melitest.core.common.viewmodels.ViewModelFactory
import co.jsilval.melitest.databinding.ProductsFragmentBinding
import co.jsilval.melitest.features.main.ui.dialogs.EmptyResultDialog
import co.jsilval.melitest.features.main.ui.dialogs.ErrorDialog
import co.jsilval.melitest.features.main.ui.dialogs.NoInternetDialog
import co.jsilval.melitest.features.products.view.adapter.history.SearchHistoryAdapter
import co.jsilval.melitest.features.products.view.adapter.history.entities.History
import co.jsilval.melitest.features.products.view.adapter.history.viewmodels.HistoryViewModel
import co.jsilval.melitest.features.products.view.adapter.products.ProductsAdapter
import co.jsilval.melitest.features.products.view.adapter.products.entities.Product
import co.jsilval.melitest.features.products.view.adapter.products.viewmodels.ProductViewModel
import co.jsilval.melitest.features.products.viewmodel.ProductsViewModel
import co.jsilval.melitest.features.products.viewmodel.ProductsViewModel.ProductsFragmentState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

/**
 * Lista de productos, inicalmente vacía, se llena después de realizar una búsqueda
 */
@ExperimentalCoroutinesApi
class ProductsFragment : BaseFragment(), ActionMode.Callback, ErrorDialog.OnErrorDialogListener,
    EmptyResultDialog.OnEmptyResultDialogListener, NoInternetDialog.OnNoInternetDialogListener {

    @Inject
    lateinit var mViewModelFactory: ViewModelFactory

    private val viewModel by viewModels<ProductsViewModel> { mViewModelFactory }
    private var _binding: ProductsFragmentBinding? = null
    private val binding get() = _binding!!

    lateinit var searchView: SearchView
    private var actionMode: ActionMode? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ProductsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerViewHistory()
        setupRecyclerViewProducts()
    }

    override fun addListeners() {
        binding.edtSearch.setOnClickListener {
            viewModel.setSearching(true)
        }
    }

    /**
     * Método para agregar observadores sobre datos del viewmodel
     */
    override fun addObservers() {
        observeState()
        observeProducts()
        observeHistory()
    }

    /**
     * Se observan los cambios en el estado de la vista. Atado al ciclo de vida del fragmento.
     * Se reciben cada cambio de estado de la vista para luego ser manejado según el nuevo estado.
     */
    private fun observeState() {
        viewModel.state
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { state ->
                handleState(state)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    /**
     * Se observan los cambios en los valores de la lista de productos a mostrar.  Atado al ciclo de vida del fragmento.
     * Cada vez que se consultan productos según un criterio de búsqueda se reciben nuevos datos y cada uno se utiliza para
     * mostrar información sobre la vista.
     */
    private fun observeProducts() {
        viewModel.products
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { products ->
                handleProductsSuccess(products)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    /**
     * Observar los nuevos cambios en el historial. Llega una lista nueva de sugerencia cada vez que se
     * escribe y está es mostrada de inmediato.
     */
    private fun observeHistory() {
        viewModel.history
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { history ->
                handleHistorySuccess(history)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    /**
     * Configuración de la lista de datos de historial de búsqueda.
     */
    private fun setupRecyclerViewHistory() {
        val adapter =
            SearchHistoryAdapter(listener = object : SearchHistoryAdapter.OnItemClickListener {
                override fun onClickHistoryItem(history: History) {
                    handleSuccessHistorySaved(history.data)
                }
            })
        binding.rvHistory.apply {
            this.adapter = adapter
        }
    }

    /**
     * Configuración de la lista de productos.
     */
    private fun setupRecyclerViewProducts() {
        val adapter =
            ProductsAdapter(listener = object : ProductsAdapter.OnItemClickListener {
                override fun onClickProductItem(product: Product) {
                    openProductDetail(product)
                }
            })
        binding.rvProducts.apply {
            this.adapter = adapter
        }
    }

    /**
     * Abre el detalle del producto seleccionado
     */
    private fun openProductDetail(product: Product) {
        goTo(
            ProductsFragmentDirections.toProductDetailFragment(product.id),
            R.id.productDetailFragment
        )
    }

    private fun fetchHistoryByQuery(query: String) {
        viewModel.fetchHistory(query)
    }

    private fun fetchProducts(query: String) {
        viewModel.fetchProducts(query)
    }

    private fun showHistory(query: String) {
        viewModel.fetchProducts(query)
    }


    /**
     * Maneja los estados de la vista
     *
     * @param state inidica el nuevo estado de la vista
     */
    private fun handleState(state: ProductsFragmentState) {
        when (state) {
            is ProductsFragmentState.IsLoading -> handleLoading(state.isLoading)
            is ProductsFragmentState.ShowToast -> requireActivity().showToast(state.message)
            is ProductsFragmentState.Init -> Unit
            is ProductsFragmentState.SuccessHistorySaved -> handleSuccessHistorySaved(state.query)
            is ProductsFragmentState.ShowErrorDialog -> handleErrorDialogs(state.errorType)
            is ProductsFragmentState.Searching -> handleSearchingState(state.isSearching)
        }
    }

    /**
     * Muestra la vista del modo "buscar productos"
     */
    private fun handleSearchingState(isSearching: Boolean) {
        if (isSearching) {
            actionMode =
                (activity as AppCompatActivity).startSupportActionMode(this@ProductsFragment)
            searchView.requestFocus()
            showHistoryList()
            searchView.showKeyboard()
        }
    }

    /**
     * Maneja los tipos de errores que pueden ocurrir en la vista
     */
    private fun handleErrorDialogs(errorType: ErrorType) {
        when (errorType) {
            ErrorType.ERROR -> showErrorDialog()
            ErrorType.EMPTY_RESULT -> showEmptyResultDialog()
            ErrorType.NO_INTERNET -> showNoInternetDialog()
        }
    }

    private fun showNoInternetDialog() {
        getDialogsManager().showDialogWithId(NoInternetDialog(), "no_internet_dialog")
    }

    private fun showEmptyResultDialog() {
        getDialogsManager().showDialogWithId(EmptyResultDialog(), "empty_result_dialog")
    }

    private fun showErrorDialog() {
        getDialogsManager().showDialogWithId(ErrorDialog(), "error_dialog")
    }

    private fun handleLoading(isLoading: Boolean) {
        when (isLoading) {
            true -> showLoadingDialog()
            false -> hideLoadingDialog()
        }
    }

    private fun hideLoadingDialog() {
        getDialogsManager().dismissCurrentlyShownDialog()
    }

    /**
     * Muestra un diálogo de carga
     */
    private fun showLoadingDialog() {
        getDialogsManager().showDialogWithId(LoadingDialog(), "loading_dialog")
    }

    /**
     * Cuando se ha guardado una sugerencia de búsqueda, se realiza el consumo del servicio de búsqueda
     * de productos.
     */
    private fun handleSuccessHistorySaved(query: String) {
        binding.edtSearch.setText(query)
        actionMode?.finish()
        fetchProducts(query)
    }

    private fun clearHistoryList() {
        (binding.rvHistory.adapter as SearchHistoryAdapter).clear()
    }

    /**
     * Maneja los nuevos datos que llegan luego de consultar exitosamente el servicio de búsqueda
     * de productos.
     */
    private fun handleProductsSuccess(products: List<ProductViewModel>) {
        (binding.rvProducts.adapter as ProductsAdapter).items = products
    }

    /**
     * Maneja los nuevos datos que llegan como sugerencia de historial de búsqueda
     */
    private fun handleHistorySuccess(history: List<HistoryViewModel>) {
        (binding.rvHistory.adapter as SearchHistoryAdapter).items = history
    }

    /**
     * Muestra la lista de Historial sugerido con una transición
     */
    private fun showHistoryList() {
        binding.motionLayout.setTransition(R.id.tsHistory)
        binding.motionLayout.transitionToEnd()
    }

    /**
     * Oculta la lista de Historial sugerido con una transición
     */
    private fun hideHistoryList() {
        binding.motionLayout.setTransition(R.id.tsHistory)
        binding.motionLayout.transitionToStart()
    }

    override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
        mode.menuInflater.inflate(R.menu.search_menu, menu)
        return true
    }

    /**
     * Antes de pasar al modo de búsqueda de productos se agrega un searchView como ActionView
     * con el estilo ya definido por xml.
     */
    override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
        val searchItem = menu.findItem(R.id.action_search)
        val actionView = searchItem.setActionView(R.layout.search_action_view)

        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = (actionView.actionView as SearchView).apply {
            maxWidth = Integer.MAX_VALUE
            setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    if (query.isNotBlank()) {
                        viewModel.saveHistory(query)
                    }
                    return false
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    fetchHistoryByQuery(newText)
                    return true
                }
            })
        }

        return true
    }

    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
        return false
    }

    override fun onDestroyActionMode(mode: ActionMode?) {
        actionMode = null
        hideKeyboard()
        hideHistoryList()
        clearHistoryList()
        binding.motionLayout.setTransition(R.id.tsProducts)
        viewModel.setSearching(false)
    }

    /**
     * Se quita el estado de error luego que se oculta el diálogo
     */
    override fun onDismissErrorDialog() {
        viewModel.clearDialogState()
    }

    /**
     * Se quita el estado de error luego que se oculta el diálogo
     */
    override fun onDismissEmptyResultDialog() {
        viewModel.clearDialogState()
    }

    /**
     * Se quita el estado de error luego que se oculta el diálogo
     */
    override fun onDismissNoInternetDialog() {
        viewModel.clearDialogState()
    }
}