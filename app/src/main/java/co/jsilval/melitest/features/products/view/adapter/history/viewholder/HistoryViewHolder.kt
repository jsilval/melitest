package co.jsilval.melitest.features.products.view.adapter.history.viewholder

import android.view.View
import android.widget.TextView
import co.jsilval.melitest.R
import co.jsilval.melitest.features.products.view.adapter.history.viewmodels.HistoryViewModel

class HistoryViewHolder(itemView: View) : AbstractViewHolder<HistoryViewModel>(itemView),
    View.OnClickListener {

    companion object {
        const val LAYOUT: Int = R.layout.history_item_layout
    }

    private val tvHistory: TextView = itemView.findViewById(R.id.tvHistory)

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        listener?.onClickHistoryItem(adapterPosition)
    }

    override fun bind(viewModel: HistoryViewModel) {
        tvHistory.text = viewModel.history.data
    }
}
