package co.jsilval.melitest.features.main.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.jsilval.melitest.core.common.dialogs.BaseFullScreenDialog
import co.jsilval.melitest.databinding.NoInternetStateLayoutBinding
import co.jsilval.melitest.features.main.ui.MainActivity

class NoInternetDialog : BaseFullScreenDialog() {

    private var _binding: NoInternetStateLayoutBinding? = null
    private val binding get() = _binding!!

    private var listener: OnNoInternetDialogListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
    }

    override fun onResume() {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        super.onResume()
        setListener()
    }

    private fun setListener() {
        val primaryNavigationFragment =
            (context as MainActivity).supportFragmentManager.primaryNavigationFragment
        val fragment = primaryNavigationFragment?.childFragmentManager?.fragments?.get(0)
        if (fragment is OnNoInternetDialogListener) {
            listener = fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = NoInternetStateLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.btnRetry.setOnClickListener {
            dismissAllowingStateLoss()
            listener?.onDismissNoInternetDialog()
        }
    }

    interface OnNoInternetDialogListener {
        fun onDismissNoInternetDialog()
    }
}