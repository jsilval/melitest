package co.jsilval.melitest.features.detail.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.jsilval.melitest.core.Result
import co.jsilval.melitest.core.common.dialogs.ErrorType
import co.jsilval.melitest.core.log.AppLog
import co.jsilval.melitest.features.detail.domain.entities.ProductDetail
import co.jsilval.melitest.features.detail.domain.usecase.GetProducts
import co.jsilval.melitest.features.detail.viewmodel.ProductDetailViewModel.ProductDetailFragmentState.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProductDetailViewModel @Inject constructor(private val getProducts: GetProducts) :
    ViewModel() {

    private val _state =
        MutableStateFlow<ProductDetailFragmentState>(ProductDetailFragmentState.Init)
    val state: StateFlow<ProductDetailFragmentState> get() = _state

    private val _product = MutableStateFlow<ProductDetail?>(null)
    val products: StateFlow<ProductDetail?> get() = _product

    private fun hideLoading() {
        _state.value = ProductDetailFragmentState.IsLoading(false)
    }

    private fun setLoading() {
        _state.value = ProductDetailFragmentState.IsLoading(true)
    }

    private fun showToast(message: String) {
        _state.value = ProductDetailFragmentState.ShowToast(message)
    }

    private fun showErrorDialog(errorType: ErrorType) {
        _state.value = ProductDetailFragmentState.ShowErrorDialog(errorType)
    }

    /**
     * Obtienen un producto según un Id. El resultado se emite desde un flow
     * La vista pasa desde el estado "Cargando" hasta el estado de éxito o error.
     */
    fun getProductById(id: String) {
        viewModelScope.launch {
            getProducts.byId(id)
                .onStart {
                    setLoading()
                }.catch { exception ->
                    hideLoading()
                    showToast(exception.message.toString())
                    AppLog.e("Error consultando producto por Id. ID: ${id}", exception)
                }.collect { result ->
                    hideLoading()
                    when (result) {
                        is Result.Error -> {
                            showErrorDialog(result.data)
                        }
                        is Result.Success -> _product.value = result.data
                    }
                }
        }
    }

    /**
     * Clase para representar estados posibles de la vista ProductDetailFragment
     * @property Init representa el estado inicial de la vista.
     * @property IsLoading indica si la vista está mostrando una progressBar
     * @property ShowToast inidica que la vista debe mostrar un mensaje en un toast.
     * @property ShowErrorDialog inidica que la vista debe mostrar un diálogo de error
     */
    sealed class ProductDetailFragmentState {
        object Init : ProductDetailFragmentState()
        data class IsLoading(val isLoading: Boolean) : ProductDetailFragmentState()
        data class ShowToast(val message: String) : ProductDetailFragmentState()
        data class ShowErrorDialog(val errorType: ErrorType) : ProductDetailFragmentState()
    }
}