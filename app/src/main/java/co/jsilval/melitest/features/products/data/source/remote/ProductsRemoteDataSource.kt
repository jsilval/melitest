package co.jsilval.melitest.features.products.data.source.remote

import android.content.Context
import co.jsilval.apimanager.core.rest.RequestCallback
import co.jsilval.apimanager.core.rest.network.ApiClient
import co.jsilval.apimanager.core.rest.network.NetworkResponse
import co.jsilval.apimanager.core.rest.network.response.ApiEmptyResponse
import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse
import co.jsilval.apimanager.core.rest.network.response.ApiSuccessResponse
import co.jsilval.melitest.core.Result
import co.jsilval.melitest.core.common.dialogs.ErrorType
import co.jsilval.melitest.core.db.products.ProductDao
import co.jsilval.melitest.core.log.AppLog
import co.jsilval.melitest.core.rest.entities.ApiSearchResponse
import co.jsilval.melitest.core.utils.ApiSearchDeserializer
import co.jsilval.melitest.core.utils.AppExecutors
import co.jsilval.melitest.core.utils.NetworkChecker
import co.jsilval.melitest.features.products.view.adapter.products.entities.Product
import co.jsilval.melitest.features.products.view.adapter.products.viewmodels.ProductViewModel
import com.google.gson.GsonBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Named
import co.jsilval.melitest.core.db.products.Product as ProductDB

/**
 * Fuente de datos remota que devuelve productos, se realiza el consumo de servicios REST
 *
 * @param searchClient cliente retrofit para consumir el servicio de búsqueda de productos
 * @param productDao entidad dao para realizar la inserción de datos a la bd
 * @param appExecutors realiza tareas de disto en 2do plano
 */
@ExperimentalCoroutinesApi
class ProductsRemoteDataSource @Inject constructor(
    @Named("search") private val searchClient: ApiClient,
    private val productDao: ProductDao,
    private val appExecutors: AppExecutors,
    private val context: Context
) {

    /**
     * Función de extensión para devuelve un flow que emite el resultado de consumir el servicio de
     * búsqueda.
     */
    private fun ApiClient.asSearchFlow(query: String) =
        callbackFlow<Result<List<ProductViewModel>, ErrorType>> {
            val listener = object : RequestCallback<String> {
                override fun responseSuccess(response: ApiSuccessResponse<String>) {
                    val gson = GsonBuilder()
                        .registerTypeAdapter(ApiSearchResponse::class.java, ApiSearchDeserializer())
                        .create()

                    val result: ApiSearchResponse =
                        gson.fromJson(response.body, ApiSearchResponse::class.java)

                    // Se convierten los datos a entidades de la capa de presentación
                    val productsDomainModel = result.result.map {
                        val product = Product(
                            it.id,
                            it.thumbnail,
                            it.price,
                            it.title,
                            it.currencyId
                        )
                        ProductViewModel(product)
                    }

                    // Se insertan los datos en la bd. Se hace en otro hilo porque este proceso
                    // es independe del resultado del resultado mostrado en la vista
                    appExecutors.diskIO().execute {
                        val productsDB = result.result.map {
                            ProductDB(
                                it.id,
                                it.thumbnail,
                                it.price,
                                it.title,
                                it.currencyId,
                                it.description
                            )
                        }
                        productDao.clearTable()
                        productDao.insert(productsDB)
                    }

                    if (productsDomainModel.isNotEmpty()) {
                        trySend(Result.Success(productsDomainModel))
                    } else {
                        trySend(Result.Error(ErrorType.EMPTY_RESULT))
                    }
                }

                // El servidor respondió con código 204.
                override fun responseEmpty(response: ApiEmptyResponse) {
                    AppLog.e("Respuesta vacía del servidor")
                    trySend(Result.Error(ErrorType.ERROR))
                }

                // El servidor respondió con dódigo distinto a 2xx.
                override fun responseError(response: ApiErrorResponse) {
                    AppLog.e("Error Obteniedo lista de productos. Query: $query \n ${response.errorMessage}")
                    trySend(Result.Error(ErrorType.ERROR))
                }

                // Ocurrió un error de red o un error inesperado.
                override fun otherError(t: Throwable) {
                    AppLog.e("Error Obteniedo lista de productos. Query: $query", t)
                    trySend(Result.Error(ErrorType.ERROR))
                }

                // Ocurrió un error de conversión de datos.
                override fun onFailure(t: Throwable) {
                    trySend(Result.Error(ErrorType.ERROR))
                }
            }

            // Antes de consumir el servicio se valida si hay conexión a internet, si no la hay se emite un
            // resultado de error.
            if (NetworkChecker.hasInternetConnected(context)) {
                searchClient.serverRequest.field("q", query)
                searchClient.execute(object : NetworkResponse<String>() {}, listener)
            } else {
                trySend(Result.Error(ErrorType.NO_INTERNET))
            }

            awaitClose { searchClient.removeListener() }
        }.flowOn(Dispatchers.IO)

    fun getProductsHistory(query: String) = searchClient.asSearchFlow(query)
}