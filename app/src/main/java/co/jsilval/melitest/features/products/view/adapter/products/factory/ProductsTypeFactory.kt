package co.jsilval.melitest.features.products.view.adapter.products.factory

import android.view.View
import co.jsilval.melitest.features.products.view.adapter.products.entities.Product
import co.jsilval.melitest.features.products.view.adapter.products.viewholder.AbstractViewHolder

interface ProductsTypeFactory {

    fun type(product: Product): Int

    fun createViewHolder(parent: View, type: Int): AbstractViewHolder<*>
}
