package co.jsilval.melitest.features.products.view.adapter.products.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import co.jsilval.melitest.R
import co.jsilval.melitest.features.products.view.adapter.products.viewmodels.ProductViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

class ProductViewHolder(itemView: View) : AbstractViewHolder<ProductViewModel>(itemView),
    View.OnClickListener {

    companion object {
        const val LAYOUT: Int = R.layout.product_item_layout
    }

    private val imageView: ImageView = itemView.findViewById(R.id.imageView)
    private val tvName: TextView = itemView.findViewById(R.id.tvDescription)
    private val tvPrice: TextView = itemView.findViewById(R.id.tvPrice)

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        listener?.onClickProductItem(adapterPosition)
    }

    override fun bind(viewModel: ProductViewModel) {
        Glide.with(itemView.context)
            .load(viewModel.product.thumbnail)
            .apply(RequestOptions.bitmapTransform(RoundedCorners(8)))
            .into(imageView)

        tvName.text = viewModel.product.title
        tvPrice.text = viewModel.product.price
    }
}
