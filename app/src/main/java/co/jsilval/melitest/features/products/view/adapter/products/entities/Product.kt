package co.jsilval.melitest.features.products.view.adapter.products.entities

data class Product(
    var id: String,
    val thumbnail: String,
    val price: String,
    val title: String,
    val currencyId: String
)