package co.jsilval.melitest.features.products.view.adapter.history

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.jsilval.melitest.features.products.view.adapter.history.entities.History
import co.jsilval.melitest.features.products.view.adapter.history.factory.SearchHistoryTypeFactory
import co.jsilval.melitest.features.products.view.adapter.history.factory.SearchHistoryTypeFactoryForList
import co.jsilval.melitest.features.products.view.adapter.history.viewholder.AbstractViewHolder
import co.jsilval.melitest.features.products.view.adapter.history.viewmodels.AbstractViewModel
import co.jsilval.melitest.features.products.view.adapter.history.viewmodels.HistoryViewModel
import kotlin.properties.Delegates

class SearchHistoryAdapter(
    private val typeFactory: SearchHistoryTypeFactory = SearchHistoryTypeFactoryForList(),
    private val listener: OnItemClickListener
) :
    RecyclerView.Adapter<AbstractViewHolder<AbstractViewModel>>(),
    AbstractViewHolder.OnItemClickedListener {

    var items: List<AbstractViewModel> by Delegates.observable(ArrayList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    lateinit var context: Context

    override fun onBindViewHolder(holder: AbstractViewHolder<AbstractViewModel>, position: Int) {
        val item = items[position]
        holder.bind(item)
        holder.setOnItemClickListener(this)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder<AbstractViewModel> {
        context = parent.context
        val contactView = LayoutInflater.from(context).inflate(viewType, parent, false)
        return typeFactory.createViewHolder(
            contactView,
            viewType
        ) as AbstractViewHolder<AbstractViewModel>
    }

    override fun getItemViewType(position: Int): Int = items[position].type(typeFactory)

    override fun getItemCount() = items.count()

    override fun onClickHistoryItem(adapterPosition: Int) {
        listener.onClickHistoryItem((items[adapterPosition] as HistoryViewModel).history)
    }

    fun clear() {
        (items as ArrayList).clear()
    }

    interface OnItemClickListener {
        fun onClickHistoryItem(history: History)
    }
}

