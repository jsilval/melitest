package co.jsilval.melitest.features.products.domain.usecase

import co.jsilval.melitest.core.Result
import co.jsilval.melitest.core.common.dialogs.ErrorType
import co.jsilval.melitest.features.products.data.source.remote.repository.ProductsRemoteRepositoryImpl
import co.jsilval.melitest.features.products.view.adapter.products.viewmodels.ProductViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * Caso de uso para manejar las búsquedas sobre un repositorio de datos remoto
 */
@ExperimentalCoroutinesApi
class ProductsByQuery @Inject constructor(private val productsRepository: ProductsRemoteRepositoryImpl) {

    suspend fun getProducts(query: String): Flow<Result<List<ProductViewModel>, ErrorType>> {
        return productsRepository.fetchProducts(query)
    }
}