package co.jsilval.melitest.features.products.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.jsilval.melitest.core.Result
import co.jsilval.melitest.core.common.dialogs.ErrorType
import co.jsilval.melitest.core.log.AppLog
import co.jsilval.melitest.features.products.domain.usecase.HistoryByQuery
import co.jsilval.melitest.features.products.domain.usecase.ProductsByQuery
import co.jsilval.melitest.features.products.view.adapter.history.viewmodels.HistoryViewModel
import co.jsilval.melitest.features.products.view.adapter.products.viewmodels.ProductViewModel
import co.jsilval.melitest.features.products.viewmodel.ProductsViewModel.ProductsFragmentState.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
class ProductsViewModel @Inject constructor(
    private val historyByQuery: HistoryByQuery,
    private val productsByQuery: ProductsByQuery
) :
    ViewModel() {

    private val _state = MutableStateFlow<ProductsFragmentState>(ProductsFragmentState.Init)
    val state: StateFlow<ProductsFragmentState> get() = _state

    private val _history = MutableStateFlow<List<HistoryViewModel>>(mutableListOf())
    val history: StateFlow<List<HistoryViewModel>> get() = _history

    private val _products = MutableStateFlow<List<ProductViewModel>>(mutableListOf())
    val products: StateFlow<List<ProductViewModel>> get() = _products

    private fun showErrorDialog(errorType: ErrorType) {
        _state.value = ProductsFragmentState.ShowErrorDialog(errorType)
    }

    private fun showToast(message: String) {
        _state.value = ProductsFragmentState.ShowToast(message)
    }

    private fun successHistorySaved(query: String) {
        _state.value = ProductsFragmentState.SuccessHistorySaved(query)
    }

    private fun setLoading() {
        _state.value = ProductsFragmentState.IsLoading(true)
    }

    private fun hideLoading() {
        _state.value = ProductsFragmentState.IsLoading(false)
    }

    /**
     * Recupera el historial según criterío de búsqueda. Los valores se emiten desde un flow
     * La vista puede entrar al estado de "Error" o "Éxito"
     *
     * @param query: Criterio de búsqueda.
     */
    fun fetchHistory(query: String) {
        viewModelScope.launch {
            historyByQuery.getHistory(query)
                .catch { exception ->
                    showToast(exception.message.toString())
                    AppLog.e("Error Obteniedo historial según palabra: Query: $query", exception)
                }
                .collect { result ->
                    when (result) {
                        is Result.Error -> showToast(result.data)
                        is Result.Success -> _history.value = result.data
                    }
                }
        }
    }

    /**
     * Guarda una palabra con la que se quiere hacer una búsqueda. Los valores se emiten desde un flow
     * La vista puede entrar al estado de "Error" o "Éxito"
     *
     * @param query: palabra clave con que se hará la búsqueda.
     */
    fun saveHistory(query: String) {
        viewModelScope.launch {
            historyByQuery.saveQuery(query)
                .catch { exception ->
                    showToast(exception.message.toString())
                    AppLog.e("Error guardando el historial. Query: $query", exception)
                }
                .collect { result ->
                    when (result) {
                        is Result.Error -> showToast(result.data)
                        is Result.Success -> {
                            hideLoading()
                            successHistorySaved(result.data)
                        }
                    }
                }
        }
    }

    /**
     * Trae la lista de productos relacionados con una palabra clave. Los valores se emiten desde un flow
     * La vista pasa desde el estado "Cargando" hasta el estado de éxito o error.
     *
     * @param query: Palabra clave para traer lista de productos relacionados.
     */
    fun fetchProducts(query: String) {
        viewModelScope.launch {
            productsByQuery.getProducts(query)
                .onStart {
                    setLoading()
                }
                .catch { exception ->
                    hideLoading()
                    showToast(exception.message.toString())
                    AppLog.e("Error Obteniedo lista de productos. Query: $query", exception)
                }.collect { result ->
                    when (result) {
                        is Result.Error -> showErrorDialog(result.data)
                        is Result.Success -> {
                            hideLoading()
                            _products.value = result.data
                        }
                    }
                }
        }
    }

    /**
     * Indica a la vista si se está buscando o no productos
     *
     * @param isSearching: Bandera para indicar el cambio de estado
     */
    fun setSearching(isSearching: Boolean) {
        _state.value = ProductsFragmentState.Searching(isSearching)
    }

    /**
     * Coloca a la vista en el estado inicial
     */
    fun clearDialogState() {
        _state.value = ProductsFragmentState.Init
    }

    /**
     * Clase para representar estados posibles de la vista ProductsFragment
     * @property Init representa el estado inicial de la vista.
     * @property SuccessHistorySaved inidica que se ha guardado el criterio de búsqueda en el historial.
     * @property IsLoading indica si la vista está mostrando un diálogo de espera
     * @property ShowToast inidica que la vista debe mostrar un mensaje en un toast.
     * @property Searching indica si la vista se encuentra buscando productos o no.
     */
    sealed class ProductsFragmentState {
        object Init : ProductsFragmentState()
        data class SuccessHistorySaved(val query: String) : ProductsFragmentState()
        data class IsLoading(val isLoading: Boolean) : ProductsFragmentState()
        data class ShowToast(val message: String) : ProductsFragmentState()
        data class ShowErrorDialog(val errorType: ErrorType) : ProductsFragmentState()
        data class Searching(val isSearching: Boolean) : ProductsFragmentState()
    }
}