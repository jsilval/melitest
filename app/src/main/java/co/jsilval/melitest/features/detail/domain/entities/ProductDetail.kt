package co.jsilval.melitest.features.detail.domain.entities

data class ProductDetail(
    val thumbnail: String,
    val price: String,
    val title: String,
    val currencyId: String,
    val detail: String
)