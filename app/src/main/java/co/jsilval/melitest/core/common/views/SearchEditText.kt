package co.jsilval.melitest.core.common.views

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.InsetDrawable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.annotation.ColorInt
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import co.jsilval.melitest.R

/**
 * Edittext personalizado con icono de lupa y botón de borrar contenido.
 */
class SearchEditText : AppCompatEditText,
    View.OnFocusChangeListener, TextWatcher {

    @ColorInt
    private var strokeColor: Int? = null
    private var isEmpty: Boolean? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    private fun init() {
        background = ContextCompat.getDrawable(context, R.drawable.search_edittext_bg)
        setCompoundDrawablesWithIntrinsicBounds(
            ContextCompat.getDrawable(
                context,
                R.drawable.ic_search
            ), null, null, null
        )
    }

    override fun performClick(): Boolean {
        return super.performClick()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val clearTextDrawable = compoundDrawables[2]
            if (clearTextDrawable != null &&
                event.rawX >= (right - (clearTextDrawable.bounds.width() + paddingRight))
            ) {
                clearContent()
            }
            isPressed = true
            return true
        } else if (event.action == MotionEvent.ACTION_UP) {
            isPressed = false
            performClick()
        }

        return super.onTouchEvent(event)
    }

    private fun clearContent() {
        setText("")
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(widthMeasureSpec, toDp(56f))
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        strokeColor = ContextCompat.getColor(context, R.color.white_alpha_80)
        onFocusChangeListener = this
        addTextChangedListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        removeTextChangedListener(this)
        onFocusChangeListener = null
    }

    override fun onTextChanged(
        text: CharSequence?,
        start: Int,
        lengthBefore: Int,
        lengthAfter: Int
    ) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter)
    }

    override fun onFocusChange(v: View, hasFocus: Boolean) {
        val strokeWidth = context.resources.getDimension(R.dimen.editText_stroke_width_1dp)
        when (hasFocus) {
            true -> animateStroke(0f, strokeWidth)
            false -> animateStroke(strokeWidth, 0f)
        }
    }

    override fun afterTextChanged(s: Editable?) {
        isEmpty = s.toString().isEmpty()
        when (isEmpty) {
            true -> hideClearTextButton()
            false -> showClearTextButton()
        }
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
    }

    private fun showClearTextButton() {
        setCompoundDrawablesWithIntrinsicBounds(
            ContextCompat.getDrawable(
                context,
                R.drawable.ic_search
            ),
            null,
            ContextCompat.getDrawable(
                context,
                R.drawable.ic_close
            ),
            null
        )
    }

    private fun showBackButton() {
        setCompoundDrawablesWithIntrinsicBounds(
            ContextCompat.getDrawable(
                context,
                R.drawable.ic_back
            ),
            null,
            ContextCompat.getDrawable(
                context,
                R.drawable.ic_close
            ),
            null
        )
    }

    private fun hideClearTextButton() {
        setCompoundDrawablesWithIntrinsicBounds(
            ContextCompat.getDrawable(
                context,
                R.drawable.ic_search
            ),
            null,
            null,
            null
        )
    }

    private fun animateStroke(start: Float, end: Float) {
        ValueAnimator.ofFloat(start, end).apply {
            duration = 300
            addUpdateListener {
                val strokeWidth = (animatedValue as Float).toInt()
                ((background as InsetDrawable).drawable as GradientDrawable).setStroke(
                    strokeWidth,
                    strokeColor!!
                )
            }
            start()
        }
    }

    fun getString() = text.toString()
}