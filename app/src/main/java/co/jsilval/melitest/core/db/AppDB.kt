package co.jsilval.melitest.core.db

import androidx.room.Database
import androidx.room.RoomDatabase
import co.jsilval.melitest.core.db.history.History
import co.jsilval.melitest.core.db.history.HistoryDao
import co.jsilval.melitest.core.db.products.Product
import co.jsilval.melitest.core.db.products.ProductDao

@Database(
    entities = [
        History::class,
        Product::class
    ],
    version = 3,
    exportSchema = false
)
abstract class AppDB : RoomDatabase() {

    abstract fun historyDao(): HistoryDao

    abstract fun productsDao(): ProductDao
}