package co.jsilval.melitest.core.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import co.jsilval.melitest.core.db.AppDB
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [
        ViewModelModule::class,
        ApiServicesModule::class]
)
class AppModule {

    @Singleton
    @Provides
    fun provideDb(app: Application): AppDB {
        return Room
            .databaseBuilder(app, AppDB::class.java, "app_db.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideAppContext(app: Application): Context = app.applicationContext

    @Singleton
    @Provides
    fun provideHistoryDao(db: AppDB) = db.historyDao()

    @Singleton
    @Provides
    fun provideProductDao(db: AppDB) = db.productsDao()
}
