package co.jsilval.melitest.core.di

import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.headers.Header
import co.jsilval.apimanager.core.rest.network.ApiClient
import co.jsilval.apimanager.core.rest.network.HttpMethod
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiServicesModule {

    @Singleton
    @Named("search")
    @Provides
    fun provideSearchClient(@Named("search") request: ServerRequest): ApiClient {
        return ApiClient(request, HttpMethod.GET)
    }

    @Singleton
    @Named("search")
    @Provides
    fun provideSearchRequest(): ServerRequest {
        return ServerRequest.create("/sites/MLA/search")
            .header(Header.CONTENT_TYPE, "application/json")
    }
}
