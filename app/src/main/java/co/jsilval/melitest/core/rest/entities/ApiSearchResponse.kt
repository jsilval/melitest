package co.jsilval.melitest.core.rest.entities

import com.google.gson.annotations.SerializedName

data class ApiSearchResponse(

	@field:SerializedName("total")
	val total: Int,

	@field:SerializedName("offset")
	val offset: Int,

	@field:SerializedName("result:")
	val result: List<ResultItem>
)

data class ResultItem(

    @field:SerializedName("available_quantity")
    val availableQuantity: Int,

    @field:SerializedName("thumbnail")
    val thumbnail: String,

    @field:SerializedName("price")
    val price: String,

    @field:SerializedName("description")
    val description: String,

    @field:SerializedName("id")
    val id: String,

    @field:SerializedName("title")
    val title: String,

    @field:SerializedName("currency_id")
    val currencyId: String
)
