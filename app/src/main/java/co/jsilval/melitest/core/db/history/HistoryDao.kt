package co.jsilval.melitest.core.db.history

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface HistoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(history: History): Long

    @Query("Select * From History Where `Query` LIKE '%' || :query || '%'")
    fun getHistoryList(query: String): List<History>

    @Query("DELETE FROM history")
    fun clearTable()
}