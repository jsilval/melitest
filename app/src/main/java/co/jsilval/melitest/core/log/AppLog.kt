package co.jsilval.melitest.core.log

import co.jsilval.melitest.BuildConfig
import co.jsilval.melitest.core.AppLoader
import co.jsilval.melitest.core.time.FastDateFormat
import co.jsilval.melitest.core.utils.AppExecutors
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStreamWriter
import java.util.*

object AppLog {
    private var streamWriter: OutputStreamWriter? = null
    private var dateFormat: FastDateFormat? = null
    private var file: File? = null
    private var initied = false
    private var appExecutors: AppExecutors? = null

    init {
        if (BuildConfig.LOGS_ENABLED) {
            init()
        }
    }

    private fun init() {
        if (initied) {
            return
        }

        appExecutors = AppExecutors()
        dateFormat = FastDateFormat.getInstance("dd-MM-yyyy_HH:mm:ss", Locale.US)

        try {
            val sdCard: File = getFile() ?: return
            val dir = File(sdCard.absolutePath + "/logs")
            dir.mkdirs()
            file =
                File(dir, dateFormat?.format(System.currentTimeMillis()).toString() + ".txt")
        } catch (e: Exception) {
            e.printStackTrace()
        }

        try {
            file!!.createNewFile()
            val stream = FileOutputStream(file)
            streamWriter = OutputStreamWriter(stream)
            streamWriter?.write(
                "-----start log ${BuildConfig.APPLICATION_ID} -- ${
                    dateFormat?.format(
                        System.currentTimeMillis()
                    )
                }-----\n"
            )
            streamWriter?.flush()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        initied = true
    }

    @ExperimentalCoroutinesApi
    private fun getFile(): File? {
        return AppLoader.weakReference?.get()?.getExternalFilesDir(null)
    }

    fun d(message: String) {
        if (!BuildConfig.LOGS_ENABLED) {
            return
        }
        init()
        Timber.d(message)
        if (streamWriter != null) {
            appExecutors?.diskIO()?.execute {
                try {
                    streamWriter?.write(
                        dateFormat?.format(
                            System.currentTimeMillis()
                        ).toString() + " Debug: " + message + "\n"
                    )
                    streamWriter?.flush()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun w(message: String) {
        if (!BuildConfig.LOGS_ENABLED) {
            return
        }
        init()
        Timber.w(message)
        if (streamWriter != null) {
            appExecutors?.diskIO()?.execute {
                try {
                    streamWriter?.write(
                        dateFormat?.format(
                            System.currentTimeMillis()
                        ).toString() + " Warning: " + message + "\n"
                    )
                    streamWriter?.flush()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun e(message: String) {
        if (!BuildConfig.LOGS_ENABLED) {
            return
        }
        init()
        Timber.e(message)
        if (streamWriter != null) {
            appExecutors?.diskIO()?.execute {
                try {
                    streamWriter?.write(
                        dateFormat?.format(
                            System.currentTimeMillis()
                        ).toString() + " Error: " + message + "\n"
                    )
                    streamWriter?.flush()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun e(message: String, exception: Throwable) {
        if (!BuildConfig.LOGS_ENABLED) {
            return
        }
        init()
        Timber.e(exception, message)
        if (streamWriter != null) {
            appExecutors?.diskIO()?.execute {
                try {
                    streamWriter?.write(
                        dateFormat?.format(
                            System.currentTimeMillis()
                        ).toString() + " Error: " + message + "\n"
                    )
                    streamWriter?.write(exception.toString())
                    val stack: Array<StackTraceElement> = exception.stackTrace
                    for (element in stack) {
                        streamWriter?.write(
                            dateFormat?.format(
                                System.currentTimeMillis()
                            ).toString() + " Error: " + element + "\n"
                        )
                    }
                    streamWriter?.flush()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }


    @ExperimentalCoroutinesApi
    fun cleanupLogs() {
        init()
        val sdCard: File = getFile() ?: return
        val dir = File(sdCard.absolutePath + "/logs")
        val files = dir.listFiles()
        if (files != null) {
            for (file in files) {
                file?.delete()
            }
        }
    }
}