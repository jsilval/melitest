package co.jsilval.melitest.core.common.fragments

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import co.jsilval.melitest.core.common.activities.BaseActivity
import co.jsilval.melitest.core.common.dialogs.DialogsManager

fun Fragment.hideKeyboard() {
    val inputMethodManager =
        activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(activity!!.window.decorView.windowToken, 0)
}

fun View.showKeyboard(): Boolean {
    try {
        val inputManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return false
}

fun BaseFragment.getDialogsManager(): DialogsManager {
    return (activity as BaseActivity).dialogsManager
}