package co.jsilval.melitest.core.common.fragments

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import co.jsilval.melitest.core.di.Injectable

abstract class BaseFragment : Fragment(), Injectable {

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        addObservers()
        addListeners()
    }

    open fun addObservers() {}

    open fun addListeners() {}

    open fun goTo(direction: NavDirections, destinationId: Int) {
        val navController = findNavController()

        navController.popBackStack(destinationId, false).let { wasPopped ->
            if (!wasPopped) {
                navController.navigate(direction)
            }
        }
    }
}