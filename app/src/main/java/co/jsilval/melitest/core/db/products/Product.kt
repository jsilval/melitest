package co.jsilval.melitest.core.db.products

import androidx.room.Entity
import co.jsilval.melitest.features.detail.domain.entities.ProductDetail

@Entity(primaryKeys = ["id"])
data class Product(
    var id: String,
    val thumbnail: String,
    val price: String,
    val title: String,
    val currencyId: String,
    val detail: String
) {
    fun toProductDetailModel() = ProductDetail(thumbnail, price, title, currencyId, detail)
}