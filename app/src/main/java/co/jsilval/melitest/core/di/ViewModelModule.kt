package co.jsilval.melitest.core.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.jsilval.melitest.core.common.viewmodels.ViewModelFactory
import co.jsilval.melitest.features.detail.viewmodel.ProductDetailViewModel
import co.jsilval.melitest.features.products.viewmodel.ProductsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ProductsViewModel::class)
    abstract fun bindProductsViewModel(viewModel: ProductsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductDetailViewModel::class)
    abstract fun bindProductDetailViewModel(viewModel: ProductDetailViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
