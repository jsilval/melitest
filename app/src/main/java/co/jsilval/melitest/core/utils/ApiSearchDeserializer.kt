package co.jsilval.melitest.core.utils

import co.jsilval.melitest.core.log.AppLog
import co.jsilval.melitest.core.rest.entities.ApiSearchResponse
import co.jsilval.melitest.core.rest.entities.ResultItem
import co.jsilval.melitest.core.utils.Utils.Companion.formatPriceString
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

/**
 * Clase para deserializar la respuesta exitosa de consumir el servicio de búsqueda de productos
 */
class ApiSearchDeserializer : JsonDeserializer<ApiSearchResponse> {

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): ApiSearchResponse? {
        val jsonObject = json.asJsonObject
        try {
            val paging = jsonObject.get("paging").asJsonObject
            val total = paging.get("total").asInt
            val offset = paging.get("offset").asInt

            val resultsArray = jsonObject.get("results").asJsonArray

            val result = resultsArray.map {
                val item = it.asJsonObject
                val id = item.get("id").asString
                val title = item.get("title").asString
                val price = item.get("price").asDouble
                val currencyId = item.get("currency_id").asString
                val thumbnail = item.get("thumbnail").asString
                val availableQuantity = item.get("available_quantity").asInt

                val description = StringBuilder()
                item.get("attributes").asJsonArray.forEach { jsonElement ->
                    try {
                        val asJsonObject = jsonElement.asJsonObject
                        val name =
                            if (asJsonObject.get("name").isJsonNull) "" else asJsonObject.get(
                                "name"
                            ).asString
                        val valueName =
                            if (asJsonObject.get("value_name").isJsonNull) "" else asJsonObject.get(
                                "value_name"
                            ).asString
                        description.append(name).append(": ").append(valueName).append("\n")
                    } catch (e: Exception) {
                        AppLog.e("Error deserializando $jsonElement", e)
                    }
                }

                ResultItem(
                    availableQuantity,
                    thumbnail,
                    formatPriceString(price),
                    description.toString(),
                    id,
                    title,
                    currencyId
                )

            }

            return ApiSearchResponse(total, offset, result)
        } catch (e: Exception) {
            AppLog.e("Error deserializando $jsonObject", e)
        }

        return null
    }
}