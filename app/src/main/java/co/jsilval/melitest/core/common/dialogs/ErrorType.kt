package co.jsilval.melitest.core.common.dialogs

enum class ErrorType {
    ERROR,
    EMPTY_RESULT,
    NO_INTERNET
}