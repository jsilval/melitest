package co.jsilval.melitest.core.db.products

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ProductDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(products: List<Product>)

    @Query("Select * From Product Where `id`= :id")
    fun getProductById(id: String): Product?

    @Query("DELETE FROM Product")
    fun clearTable()
}