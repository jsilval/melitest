package co.jsilval.melitest.core.utils

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

sealed class Utils {
    companion object {
        /**
         * Formatea un tipo de dato double a un valor monetario con el signo de peso
         *
         * @param value valor a formatear
         */
        fun formatPriceString(value: Double): String {
            val otherSymbols = DecimalFormatSymbols(Locale("es"))
            otherSymbols.decimalSeparator = ','
            val pricePattern = "$ #,###,###,###.####"
            val format = DecimalFormat(pricePattern, otherSymbols)
            return format.format(value)
        }
    }
}