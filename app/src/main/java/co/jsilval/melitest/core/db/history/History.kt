package co.jsilval.melitest.core.db.history

import androidx.room.Entity
import co.jsilval.melitest.features.products.view.adapter.history.entities.History
import co.jsilval.melitest.features.products.view.adapter.history.viewmodels.HistoryViewModel

@Entity(primaryKeys = ["query"])
data class History(
    val query: String,
) {
    fun toDomainModelHistory(): HistoryViewModel {
        return HistoryViewModel(History(query))
    }
}