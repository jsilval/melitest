package co.jsilval.melitest.core.utils

import android.content.Context
import android.net.ConnectivityManager
import co.jsilval.melitest.core.common.REACHABLE_SERVER
import co.jsilval.melitest.core.log.AppLog
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

/**
 * Clase para
 */
object NetworkChecker {

    /**
     * Verifica si la red está activa.
     */
    private fun hasNetworkAvailable(context: Context): Boolean {
        val service = Context.CONNECTIVITY_SERVICE
        val manager = context.getSystemService(service) as ConnectivityManager?
        val network = manager?.activeNetworkInfo
        AppLog.d("hasNetworkAvailable: ${(network != null)}")
        return (network != null)
    }

    /**
     * Comprueba que la res esté activa y si es el caso realiza una petición a google
     * para comprobar si hay conexión a internet.
     */
    fun hasInternetConnected(context: Context): Boolean {
        if (hasNetworkAvailable(context)) {
            try {
                val connection = URL(REACHABLE_SERVER).openConnection() as HttpURLConnection
                connection.setRequestProperty("User-Agent", "ConnectionTest")
                connection.setRequestProperty("Connection", "close")
                connection.connectTimeout = 1000
                connection.connect()
                AppLog.d("hasInternetConnected: ${(connection.responseCode == 200)}")
                return (connection.responseCode == 200)
            } catch (e: IOException) {
                AppLog.e("Error checking internet connection", e)
            }
        } else {
            AppLog.w("No network available!")
        }
        AppLog.d("hasInternetConnected: false")
        return false
    }
}