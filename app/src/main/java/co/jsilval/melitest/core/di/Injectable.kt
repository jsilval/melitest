package co.jsilval.melitest.core.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
