package co.jsilval.melitest.core.di

import co.jsilval.melitest.features.detail.view.ProductDetailFragment
import co.jsilval.melitest.features.products.view.ProductsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@Module
abstract class FragmentBindingModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeProductsFragment(): ProductsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeProductDetailFragment(): ProductDetailFragment
}