package co.jsilval.melitest.core

/**
 * Clase que mantiene el estado de un valor devuelto desde un repositorio de datos.
 * @param <R> tipo de dato devuelto en un estado de éxito.
 * @param <K> tipo de dato devuelto en un estado de error.
 *
 * @property Success inidica que ha sido exitoso el resultado.
 * @property Error inidica que ha ocurrido un error.
 */
sealed class Result<out R : Any, out K : Any> {
    data class Success<T : Any>(val data: T) : Result<T, Nothing>()
    data class Error<K : Any>(val data: K) : Result<Nothing, K>()
}