package co.jsilval.melitest.core.common.dialogs

import android.view.WindowManager
import androidx.fragment.app.DialogFragment

open class BaseFullScreenDialog : DialogFragment() {

    override fun onResume() {
        val params = dialog?.window?.attributes
        params?.width = WindowManager.LayoutParams.MATCH_PARENT
        params?.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog?.window?.attributes = params
        super.onResume()
    }
}