package co.jsilval.melitest.core.di

import co.jsilval.melitest.features.main.di.MainActivityModule
import co.jsilval.melitest.features.main.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentBindingModule::class, MainActivityModule::class])
    abstract fun contributeMainActivity(): MainActivity
}
