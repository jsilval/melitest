package co.jsilval.melitest.core.common.views

import android.view.View
import kotlin.math.ceil

fun View.toDp(value: Float): Int {
    val density = context.resources.displayMetrics.density
    return if (value == 0f) {
        0
    } else ceil((density * value).toDouble()).toInt()
}